# game code challenge (client)
Based on  [angularjs-webpack](https://github.com/preboot/angularjs-webpack)

```bash
# install the dependencies with npm
$ npm install

# start the server
$ npm start
```

Change the api url in the _src/app/app/config.js_
