export default class RegistrationCtrl {
    constructor($http, $state, config) {
        Object.assign(this, {$http, $state, config})
    }
    submit(data) {
        const {$http, $state, config} = this
        $http
            .post(config.url.registration, data)
            .then(
                ({data}) => $state.go('app.login'),
                response => this.errors = response
            )
    }
}
