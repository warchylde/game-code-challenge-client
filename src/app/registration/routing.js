import component from './component'

export default function Config($stateProvider) {
    $stateProvider.state({
        name: 'app.registration',
        url: `/${component.name}`,
        component: component.name
    })
}
