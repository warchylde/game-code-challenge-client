import angular from 'angular'
import component from './component'
import routing from './routing'

export default angular
  .module('app.registration', [])
  .config(routing)
  .component(component.name, component)
