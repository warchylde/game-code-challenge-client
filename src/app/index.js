import angular from 'angular'
import 'angular-resource'
import uirouter from 'angular-ui-router'
import '../style/app.css'
import app from './app/index'
import crossroads from './crossroads/index'
import login from './login/index'
import registration from './registration/index'
import character from './char/index'
import game from './game/index'

angular.module('app', [
    'ngResource',
    uirouter,
    app.name,
    login.name,
    crossroads.name,
    registration.name,
    character.name,
    game.name
])
