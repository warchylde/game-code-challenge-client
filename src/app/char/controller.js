export default class CharCtrl {
    constructor($q, $state, config, CharResource) {
        Object.assign(this, {
            $q,
            $state,
            config,
            CharResource,
            chars: []
        })

        this.load()
    }

    load() {
        this.loading = true
        this.CharResource
            .query()
            .$promise
            .then(chars => this.chars = chars)
            .finally(() => this.loading = false)
    }

    create(name) {
        this.CharResource
            .save({name})
            .$promise
            .then(char => this.chars.push(char))
    }

    remove(char) {
        char
            .$remove()
            .then(() => this.load())
    }

}
