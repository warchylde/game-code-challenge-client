import controller from './controller'
import template from './template.html'

export default {
    name: 'char',
    template,
    controller
};
