export default function CharResource($resource, config) {
    return $resource(`${config.url.char}/:id`, {id: '@id'})
}
