import angular from 'angular'
import component from './component'
import routing from './routing'
import resource from './resource'

export default angular
  .module('app.char', [])
  .config(routing)
  .factory(resource.name, resource)
  .component(component.name, component)
