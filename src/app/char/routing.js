import component from './component'

export default function Config($stateProvider) {
    $stateProvider.state({
        name: 'app.restricted.char',
        url: `/${component.name}`,
        component: component.name
    })
}
