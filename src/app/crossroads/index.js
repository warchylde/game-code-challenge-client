import angular from 'angular'
import routing from './routing'
import component from './component'

export default angular
  .module('app.crossroads', [])
  .config(routing)
  .component(component.name, component)
