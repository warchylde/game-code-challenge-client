import controller from './controller'
import template from './template.html'

export default {
    name: 'crossroads',
    template,
    controller
};
