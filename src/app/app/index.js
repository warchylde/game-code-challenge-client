import angular from 'angular'
import config from './config'
import run from './run'
import routing from './routing'
import component from './component'
import service from './service'
import authorize from './authorize'

export default angular
    .module('app.app', [])
    .constant('config', config)
    .run(run)
    .config(authorize)
    .config(routing)
    .component(component.name, component)
    .factory(service.name, service)
