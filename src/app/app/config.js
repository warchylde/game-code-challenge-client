const BASE_URL = window.BASE_URL || 'http://127.0.0.1:8000'

export default {
    url: {
        BASE_URL,
        registration: BASE_URL + '/user/register',
        login: BASE_URL + '/user/login',
        char: BASE_URL + '/char',
        game: BASE_URL + '/game',
    },
    status: {
        newborn: 'newborn'
    }
}
