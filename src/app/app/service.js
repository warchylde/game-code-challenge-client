export default function App() {
    let token = localStorage.token

    return {
        setToken(v) {
            token = v
            localStorage.token = token
        },
        getToken() {
            return token
        },
        hasToken() {
            return !!token
        }
    }
}
