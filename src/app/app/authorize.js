export default function config($httpProvider) {
    $httpProvider.interceptors.push(interceptor)
}

function interceptor(config, $injector) {
    return {
        request(request) {
            const App = $injector.get('App')
            if (isToAPI(request) && App.hasToken() && !request.headers.Authorization) {
                request.headers.Authorization = `Bearer ${App.getToken()}`
            }
            return request
        }
    }

    /////////////////
    function isToAPI(request) {
        return (request.url || request.config.url).indexOf(config.url.BASE_URL) === 0
    }
}