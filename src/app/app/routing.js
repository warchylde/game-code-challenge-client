import component from './component'

export default function Config($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise(`/ng/app/crossroads`)
    $locationProvider.html5Mode(true)

    $stateProvider
        .state({
            name: 'app',
            url: `/ng/${component.name}`,
            component: component.name,
            abstract: true
        })
        .state({
            name: 'app.restricted',
            url: `/restricted`,
            template: '<ui-view></ui-view>',
            abstract: true
        })

}
