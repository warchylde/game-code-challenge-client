export default function run($transitions) {
    $transitions.onStart({to: 'app.restricted.**'}, function (trans) {
        const app = trans.injector().get('App')

        if (!app.hasToken()) {
            return trans.router.stateService.target('app.login');
        }
    })
}