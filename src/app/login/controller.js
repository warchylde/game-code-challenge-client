export default class LoginCtrl {
    constructor($http, $state, Base64, config, App) {
        Object.assign(this, {
            $http,
            $state,
            Base64,
            config,
            App,
            form: {
                username: 'test',
                password: '2test123'
            }
        })
    }
    submit(form) {
        const {$http, $state, Base64, config, App} = this

        const authdata = Base64.encode(form.username + ':' + form.password)

        $http
            .post(config.url.login, form, {headers: {Authorization: 'Basic ' + authdata}})
            .then(
                ({data: {token}}) => {
                    App.setToken(token)
                    $state.go('app.restricted.char')
                },
                r => console.info('fail', r)
            )
    }
}
