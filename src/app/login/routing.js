import component from './component'

export default function Config($stateProvider) {
    $stateProvider.state({
        name: 'app.login',
        url: `/${component.name}`,
        component: component.name
    })
}
