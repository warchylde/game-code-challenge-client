import angular from 'angular'
import component from './component'
import routing from './routing'
import service from './service'

export default angular
  .module('app.login', [])
  .config(routing)
  .component(component.name, component)
  .factory(service.name, service)
