import './style.css'
import angular from 'angular'
import component from './component'
import routing from './routing'
import gameObject from './game-object'
import resource from './resource'

export default angular
    .module('app.game', [])
    .config(routing)
    .factory(resource.name, resource)
    .component(component.name, component)
    .directive(gameObject.name, gameObject)
