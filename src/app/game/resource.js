export default function GameResource($resource, config) {
    const actions = ['left', 'right', 'up', 'down'].reduce((actions, dir) => Object.assign(actions, {
            [`move${firstUpper(dir)}`]: {
                method: 'put',
                params: {dir}
            }
        }),
        {
            status: {method: 'get'},
            generate: {method: 'post'}
        })

    return id => $resource(`${config.url.game}/${id}/:dir`, {dir: '@dir'}, actions)

    ////////////////////////////////////////
    function firstUpper(str) {
        return `${str.slice(0, 1).toUpperCase()}${str.slice(1)}`
    }
}
