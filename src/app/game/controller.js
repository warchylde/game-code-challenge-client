const KEY_CODE_LEFT = 37
const KEY_CODE_RIGHT = 39
const KEY_CODE_UP = 38
const KEY_CODE_DOWN = 40

const METHOD_MAP = {
    [KEY_CODE_LEFT]: '$moveLeft',
    [KEY_CODE_RIGHT]: '$moveRight',
    [KEY_CODE_UP]: '$moveUp',
    [KEY_CODE_DOWN]: '$moveDown'
}

export default class GameCtrl {
    constructor($q, $document, $stateParams, GameResource, config) {
        Object.assign(this, {
            $q,
            $document,
            config,
            loading: true,
            game: GameResource($stateParams.id).status(game => this.init(game)),
            onKeydown: this.getOnKeydown()
        })
    }

    init(game) {
        const {$q, config} = this
        const promise = game.character.status === config.status.newborn
            ? game.$generate()
            : $q.resolve()

        promise. finally(() => this.loading = false)
    }

    getOnKeydown() {
        return ({keyCode}) => {
            const method = METHOD_MAP[keyCode]

            if (method) {
                this.game[method]()
            }
        }
    }

    $postLink() {
        this.$document.on('keydown', this.onKeydown)
    }

    $onDestroy() {
        this.$document.off('keydown', this.onKeydown)
    }
}
