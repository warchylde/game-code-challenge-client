import component from './component'

export default function Config($stateProvider) {
    $stateProvider.state({
        name: 'app.restricted.game',
        url: `/char/{id}`,
        component: component.name
    })
}
