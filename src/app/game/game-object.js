export default function gameObject() {
    return {
        link($scope, $el, $attr) {
            const scale = 5;

            $scope.$watch($attr.model, v => {
                $el.css({
                    left: `${v.x * scale}px`,
                    top: `${v.y * scale}px`                    
                })
            }, true)
        }
    }
}
